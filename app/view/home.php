<div id="section1">
	<div class="row">
		<h2>We Serve</h2>
		<div class="container flex-jspacea">
			<dl>
				<dt> <img src="public/images/content/serve1.jpg" alt="commercial"> </dt>
				<dd>
					<h4>COMMERCIAL / INDUSTRIAL <br> ACCOUNTS</h4>
					<p>We offer the largest range of products and options to make your workspace comfortable and something to be proud of. We always strive for your complete satisfaction, and completion of the task promptly.</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/serve2.jpg" alt="residential"> </dt>
				<dd>
					<h4>RESIDENTIAL CLIENTS</h4>
					<p>In beautifying your home, our complete selection of custom-ordered products is available for you to choose from. We also offer optional installation, alteration, and repairs for these products, if it should be required. We will work with you, our cherished customer from doing up a quote to measuring and consultation, to having the product properly installed.</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/serve3.jpg" alt="custom"> </dt>
				<dd>
					<h4>CUSTOM APPLICATIONS</h4>
					<p>Challenging windows of odd shapes, sizes and in hard to reach locations are our specialty. If you want to cover it and/or make it functional, we can suggest the most appropriate and the most economical of solutions.</p>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<h3>ABOUT</h3>
		<div class="container">
			<div class="left">
				<h2>DENNIS SCOTT BLINDS<br>
					AND SHUTTERS</h2>
				<p>Our mission, for well over 35 years, has always been to provide you, our valued customer, with the best products for the job, at the most reasonable of prices, backed by our available installation. Our aim is to always provide you with prompt, knowledgeable and professional service that we can be can be proud of. Over ten thousand customers served, with many coming back to us for their next project and/or recommending us to their friends and family, is a testament to how closely we have adhered to that mission. </p>
				<p>At Dennis Scott Blinds, we specialize in the services that will make your home special and unique by offering window coverings that are both custom ordered and/or custom cut, equipped standard controls, motorization, and/or cordless.</p>
				<p>We work diligently with you, to a provide premium service at very competitive rates. From design innovation and installation expertise, to customer focused service, we surpass the competition, every time, year after year.<br>
				Our team of highly trained and skilled industry professionals are waiting to get started on your unique project. So please do contact us today, and experience the Dennis Scott Blinds difference!
				</p>
				<div class="links">
					<a href="services#content" class="btn">READ MORE</a>
					<p class="sm">
						<a href="<?php $this->info("fb_link"); ?>" class="socialico" target="_blank">F</a>
						<a href="<?php $this->info("tt_link"); ?>" class="socialico" target="_blank">L</a>
						<a href="<?php $this->info("yt_link"); ?>" class="socialico" target="_blank">X</a>
						<a href="<?php $this->info("rs_link"); ?>" class="socialico" target="_blank">R</a>
					</p>
				</div>
				<p>
					CALL OR TEXT US TODAY!
					<span><?php $this->info(["phone","tel"]); ?></span>
					<span><?php $this->info(["email","mailto"]); ?></span>
				</p>
			</div>
			<div class="right">
				<img src="public/images/content/about.jpg" alt="about" class="mobile-img">
			</div>
		</div>

	</div>
</div>
