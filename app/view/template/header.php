<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="hdTop">
				<div class="row mobile-flex-jspacea">
					<p>
						<a href="<?php $this->info("fb_link"); ?>" class="socialico" target="_blank">F</a>
						<a href="<?php $this->info("tt_link"); ?>" class="socialico" target="_blank">L</a>
						<a href="<?php $this->info("yt_link"); ?>" class="socialico" target="_blank">X</a>
						<a href="<?php $this->info("rs_link"); ?>" class="socialico" target="_blank">R</a>
					</p>
					<p>
						EMAIL:
						<span><?php $this->info(["email","mailto"]); ?></span>
					</p>
					<p>
						PHONE / TEXT:
						<span><?php $this->info(["phone","mailto"]); ?></span>
					</p>
					<p>
						TOLL FREE:
						<span><?php $this->info("toll"); ?></span>
					</p>
				</div>
			</div>
			<div class="hdBot">
				<div class="row mobile-flex-jspacea mobile-flex-column">
					<div class="left">
						<a href="<?php echo URL; ?>">
							<img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Main Logo" class="hdLogo">
						</a>
					</div>
					<div class="right">
						<nav>
							<a href="#" id="pull"><strong>MENU</strong></a>
							<ul>
								<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
								<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
								<li <?php $this->helpers->isActiveMenu("specials"); ?>><a href="<?php echo URL ?>specials#content">SPECIALS</a></li>
								<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
								<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">PRIVACY POLICY</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php //if($view == "home"):?>
		<div id="banner">
			<div class="row">
			</div>
		</div>
		<div id="bannerBot">
			<div class="row">
				<h4>Your Premier Local Window Covering Specialists, for over 35 years</h4>
				<h1>Let us help you tame your unmanageable windows.</h1>
				<h3>CALL / TEXT DENNIS SCOTT BLINDS AT (604) 850-0556</h3>
				<a href="contact#content" class="btn">FREE QUOTES</a>
				<p><strong>+</strong> FREE IN-HOME MEASURES and CONSULTATION, with your order.</p>
			</div>
		</div>
	<?php //endif; ?>
